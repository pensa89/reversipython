from PPlay.gameimage import GameImage
from PPlay.window import *
from util.Util import Util
from util import Singleton
from jogador.Humano import Humano
from jogador.Computador import Computador
from tabuleiro.ChecaVitoria import ChecaVitoria


def menu():
    global estado_jogo
    altura_janela = janela.height
    largura_janela = janela.width

    jogar = GameImage("../Imagens/menu/jogar-menu.png")
    sair = GameImage("../Imagens/menu/sair-menu.png")

    jogar.set_position((largura_janela - jogar.width)/2, (altura_janela - jogar.height)/2)
    sair.set_position((largura_janela - sair.width)/2, (altura_janela - sair.height)/2 + (0.1 * altura_janela))

    jogar.draw()
    sair.draw()

    if janela.get_mouse().is_over_object(sair) and janela.get_mouse().is_button_pressed(1):
        janela.close()
    if janela.get_mouse().is_over_object(jogar) and janela.get_mouse().is_button_pressed(1):
        janela.clear()
        estado_jogo = 1
        

def pre_jogo():
    global cor_jogador
    global cor_computador
    global estado_jogo
    global humano
    global computador

    altura_janela = janela.height
    largura_janela = janela.width

    selecione_cor = GameImage("../Imagens/cores/selecione-cor.png")
    jogar = GameImage("../Imagens/cores/jogar-cores.png")
    voltar = GameImage("../Imagens/cores/voltar-cores.png")
    cor_branca = GameImage("../Imagens/cores/branco.png")
    cor_preta = GameImage("../Imagens/cores/preto.png")

    selecione_cor.set_position((largura_janela - selecione_cor.width)/2, (altura_janela - selecione_cor.height)/2 - (0.3 * altura_janela))
    jogar.set_position((largura_janela - jogar.width)/2, (altura_janela - jogar.height)/2 + (0.3 * altura_janela))
    voltar.set_position((largura_janela - voltar.width)/2, (altura_janela - voltar.height)/2 + (0.4 * altura_janela))
    cor_branca.set_position((largura_janela - cor_branca.width)/2, (altura_janela - cor_branca.height)/2)
    cor_preta.set_position((largura_janela - cor_preta.width)/2, (altura_janela - cor_preta.height)/2 - (0.1 * altura_janela))

    selecione_cor.draw()
    jogar.draw()
    voltar.draw()
    cor_branca.draw()
    cor_preta.draw()

    if janela.get_mouse().is_over_object(cor_preta) and janela.get_mouse().is_button_pressed(1):
        cor_jogador = -1
        cor_computador = 1
    if janela.get_mouse().is_over_object(cor_branca) and janela.get_mouse().is_button_pressed(1):
        cor_jogador = 1
        cor_computador = -1
    if cor_jogador != 0 and janela.get_mouse().is_over_object(jogar) and janela.get_mouse().is_button_pressed(1):
        janela.clear()
        humano.cor = cor_jogador
        computador.cor = cor_computador
        estado_jogo = 2
    if janela.get_mouse().is_over_object(voltar) and janela.get_mouse().is_button_pressed(1):
        janela.clear()
        estado_jogo = 0


def jogo():
    global estado_jogo
    pecas = Singleton.tabuleiro.matriz

    altura_janela = janela.height
    largura_janela = janela.width

    tabuleiro = GameImage("../Imagens/jogo/tabuleiro.png")
    turno_branca = GameImage("../Imagens/jogo/turno-branca.png")
    turno_preta = GameImage("../Imagens/jogo/turno-preta.png")
    tabuleiro.set_position((largura_janela - tabuleiro.width)/2, (altura_janela - tabuleiro.height)/2)
    turno_branca.set_position((largura_janela - turno_branca.width)/2, (altura_janela - turno_branca.height)/2 - (0.4 * altura_janela))
    turno_preta.set_position((largura_janela - turno_preta.width)/2, (altura_janela - turno_preta.height)/2 - (0.39 * altura_janela))

    linha, coluna = Util.determina_posicao_clique(tabuleiro.x, tabuleiro.y, janela.get_mouse().get_position())
    linha = int(linha)
    coluna = int(coluna)

    if cor_jogador == Singleton.tabuleiro.turno and Util.checa_limites_matriz(pecas, linha, coluna) and \
           janela.get_mouse().is_button_pressed(1):
        humano.faz_jogada_humano(Singleton.tabuleiro, linha, coluna)
    elif cor_computador == Singleton.tabuleiro.turno:
        proxima_jogada = computador.faz_jogada_computador(Singleton.tabuleiro)
        if proxima_jogada is not None and proxima_jogada.acao is not None:
            x, y = proxima_jogada.acao.get_ponto()
            computador.faz_jogada(Singleton.tabuleiro, x, y)
        Singleton.tabuleiro.turno *= -1

    tabuleiro.draw()
    for i in range(Singleton.tabuleiro.tamanho):
        for j in range(Singleton.tabuleiro.tamanho):
            if pecas[i][j] == 1:
                # desenha uma peça branca
                peca_branca = GameImage("../Imagens/jogo/peca_branca.png")
                x = i * peca_branca.width + tabuleiro.x
                y = j * peca_branca.height + tabuleiro.y
                peca_branca.set_position(x, y)
                peca_branca.draw()
            elif pecas[i][j] == -1:
                # desenha uma peça preta
                peca_preta = GameImage("../Imagens/jogo/peca_preta.png")
                x = i * peca_preta.width + tabuleiro.x
                y = j * peca_preta.height + tabuleiro.y
                peca_preta.set_position(x, y)
                peca_preta.draw()

    if Singleton.tabuleiro.turno == -1:
        turno_preta.draw()
    else:
        turno_branca.draw()

    if ChecaVitoria.checa_vitoria(Singleton.tabuleiro) != -sys.maxsize:
        janela.clear()
        estado_jogo = 3


def pos_jogo():
    global estado_jogo

    altura_janela = janela.height
    largura_janela = janela.width

    vitoria_preta = GameImage("../Imagens/game-over/vitoria-preta.png")
    vitoria_branca = GameImage("../Imagens/game-over/vitoria-branca.png")
    empate = GameImage("../Imagens/game-over/empate.png")
    tentar_novamente = GameImage("../Imagens/game-over/tentar-novamente.png")
    sair = GameImage("../Imagens/game-over/sair-game-over.png")

    vitoria_preta.set_position((largura_janela - vitoria_preta.width)/2, (altura_janela - vitoria_preta.height)/2 - (0.1 * altura_janela))
    vitoria_branca.set_position((largura_janela - vitoria_branca.width)/2, (altura_janela - vitoria_branca.height)/2 - (0.1 * altura_janela))
    empate.set_position((largura_janela - empate.width)/2, (altura_janela - empate.height)/2 - (0.1 * altura_janela))
    tentar_novamente.set_position((largura_janela - tentar_novamente.width)/2, (altura_janela - tentar_novamente.height)/2 + (0.2 * altura_janela))
    sair.set_position((largura_janela - sair.width)/2, (altura_janela - sair.height)/2 + (0.3 * altura_janela))

    if ChecaVitoria.checa_vitoria(Singleton.tabuleiro) == -1:
        vitoria_preta.draw()
    elif ChecaVitoria.checa_vitoria(Singleton.tabuleiro) == 1:
        vitoria_branca.draw()
    else:
        empate.draw()
    tentar_novamente.draw()
    sair.draw()

    if janela.get_mouse().is_over_object(sair) and janela.get_mouse().is_button_pressed(1):
        janela.close()
    if janela.get_mouse().is_over_object(tentar_novamente) and janela.get_mouse().is_button_pressed(1):
        janela.clear()
        estado_jogo = 1


janela = Window(800, 600)
janela.set_title("Reversi")
mouse = janela.get_mouse()
background = GameImage("../Imagens/background/table5.png")

executando = True
cor_jogador = 0
cor_computador = 0
estado_jogo = 0
humano = Humano(cor_jogador)
computador = Computador(cor_computador, 6)

pecas = [[0 for x in range(8)] for x in range(8)]
pecas[3][3] = 1
pecas[4][4] = 1
pecas[3][4] = -1
pecas[4][3] = -1
Singleton.tabuleiro.matriz = pecas

while executando:

    background.draw()
    if estado_jogo == 0:
        menu()
    elif estado_jogo == 1:
        pre_jogo()
    elif estado_jogo == 2:
        jogo()
    elif estado_jogo == 3:
        pos_jogo()

    janela.update()
