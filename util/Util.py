from builtins import staticmethod
from tabuleiro.Tabuleiro import Tabuleiro

class Util:

    @staticmethod
    def copia_profunda(tabuleiro):
        novo_tabuleiro = Tabuleiro()
        novo_tabuleiro.turno = tabuleiro.turno
        for i in range(8):
            for j in range(8):
                novo_tabuleiro.matriz[i][j] = tabuleiro.matriz[i][j]
        return novo_tabuleiro

    @staticmethod
    def determina_posicao_clique(x_tabuleiro, y_tabuleiro, coords_mouse):
        linha = (coords_mouse[0] - x_tabuleiro) / 50
        coluna = (coords_mouse[1] - y_tabuleiro) / 50
        return linha, coluna

    @staticmethod
    def checa_limites(matriz, linha, coluna):
        return 0 <= linha < len(matriz) and 0 <= coluna < len(matriz)

    @staticmethod
    def sinal(numero):
        if numero < 0:
            return -1
        elif numero > 0:
            return +1
        return 0

    @staticmethod
    def checa_limites(valor, incremento):
        tamanho_tabuleiro = 8
        return (valor + incremento < tamanho_tabuleiro) and (valor + incremento >= 0)

    @staticmethod
    def checa_limites_matriz(matriz, linha, coluna):
        return linha >= 0 and linha < len(matriz) and coluna >= 0 and coluna < len(matriz)
