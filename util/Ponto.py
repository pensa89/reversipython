class Ponto:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_ponto(self):
        return self.x, self.y

    def set_ponto(self, x, y):
        self.x = x
        self.y = y
