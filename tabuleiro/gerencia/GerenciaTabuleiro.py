from util.Ponto import Ponto
from util.Util import Util

class GerenciaTabuleiro:

    def __init__(self, tabuleiro):
        self.tabuleiro = tabuleiro

    def sinal(self, numero):
        if numero < 0:
            return -1
        if numero > 0:
            return +1
        return 0

    def gera_movimentos_possiveis(self, cor):
        movimentos_possiveis = set()
        for i in range(self.tabuleiro.tamanho):
            for j in range(self.tabuleiro.tamanho):
                if self.tabuleiro.matriz[i][j] == 0 and self.checa_vizinhanca_por_pecas(i, j):
                    ponto_tabuleiro = Ponto(i, j) # ponto que representa uma posição possível no tabuleiro
                    movimentos_possiveis.add(ponto_tabuleiro)
        movimentos_validos = set()
        for ponto in set(movimentos_possiveis):
            x, y = ponto.get_ponto()
            bool1 = self.checa_movimentos_vertical(cor, x, y)
            bool2 = self.checa_movimentos_horizontal(cor, x, y)
            bool3 = self.checa_movimentos_diagonal_principal(cor, x, y)
            bool4 = self.checa_movimentos_diagonal_secundaria(cor, x, y)
            if bool1 or bool2 or bool3 or bool4:
                ponto_valido_tabuleiro = Ponto(x, y) # ponto que representa uma posição válida no tabuleiro
                movimentos_validos.add(ponto_valido_tabuleiro)
        return movimentos_validos

    def checa_vizinhanca_por_pecas(self, linha, coluna):
        for i in range(-1, 2):
            for j in range(-1, 2):
                if linha + i >= 0 and linha + i < self.tabuleiro.tamanho and coluna + j >= 0 and coluna + j < self.tabuleiro.tamanho and self.tabuleiro.matriz[linha + i][coluna + j] != 0:
                    return True
        return False

    def checa_limites(self, valor, incremento):
        return (valor + incremento < self.tabuleiro.tamanho) and (valor + incremento >= 0)

    def checa_movimentos_vertical(self, cor, linha, coluna):
        incremento = 1
        while Util.checa_limites(linha, incremento) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento][coluna]) == -Util.sinal(cor):
            incremento += 1
        if Util.checa_limites(linha, incremento) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento][coluna]) == Util.sinal(cor) and incremento != 1:
            return True

        incremento = -1
        while Util.checa_limites(linha, incremento) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento][coluna]) == -Util.sinal(cor):
            incremento -= 1
        return Util.checa_limites(linha, incremento) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento][coluna]) == Util.sinal(cor) and incremento != -1

    def checa_movimentos_horizontal(self, cor, linha, coluna):
        incremento = 1
        while Util.checa_limites(coluna, incremento) and \
                Util.sinal(self.tabuleiro.matriz[linha][coluna + incremento]) == -Util.sinal(cor):
            incremento += 1
        if Util.checa_limites(coluna, incremento) and \
                Util.sinal(self.tabuleiro.matriz[linha][coluna + incremento]) == Util.sinal(cor) and incremento != 1:
            return True

        incremento = -1
        while Util.checa_limites(coluna, incremento) and \
                Util.sinal(self.tabuleiro.matriz[linha][coluna + incremento]) == -Util.sinal(cor):
            incremento -= 1
        return Util.checa_limites(coluna, incremento) and \
               Util.sinal(self.tabuleiro.matriz[linha][coluna + incremento]) == Util.sinal(cor) and incremento != -1

    def checa_movimentos_diagonal_principal(self, cor, linha, coluna):
        incremento_linha = 1
        incremento_coluna = 1
        while Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == -Util.sinal(cor):
            incremento_linha += 1
            incremento_coluna += 1
        if Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(cor) and \
                incremento_linha != 1 and incremento_coluna != 1:
            return True

        incremento_linha = -1
        incremento_coluna = -1
        while Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == -Util.sinal(cor):
            incremento_linha -= 1
            incremento_coluna -= 1
        return Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(cor) and \
                incremento_linha != -1 and incremento_coluna != -1

    def checa_movimentos_diagonal_secundaria(self, cor, linha, coluna):
        incremento_linha = 1
        incremento_coluna = -1
        while Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == -Util.sinal(cor):
            incremento_linha += 1
            incremento_coluna -= 1
        if Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(cor) and \
                incremento_linha != 1 and incremento_coluna != -1:
            return True

        incremento_linha = -1
        incremento_coluna = 1
        while Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == -Util.sinal(cor):
            incremento_linha -= 1
            incremento_coluna += 1
        return Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                Util.sinal(self.tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(cor) and \
                incremento_linha != -1 and incremento_coluna != 1


