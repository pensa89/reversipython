from util.Util import Util


class EstabilidadePecas:

    @staticmethod
    def reseta_tabuleiro(tabuleiro):
        for i in range(0, 8):
            for j in range(0, 8):
                valor_peca = tabuleiro.matriz[i][j]
                if valor_peca % 5 == 0 and valor_peca % 25 != 0 and valor_peca != 0:
                    tabuleiro.matriz[i][j] = valor_peca/5
        tabuleiro.alterado = False

    @staticmethod
    def pecas_estaveis(self, tabuleiro):
        for i in range(0, 8):
            for j in range(0, 8):
                if tabuleiro.alterado:
                    self.reseta_tabuleiro(tabuleiro)
                self.verifica_peca(tabuleiro, i, j)
        return tabuleiro

    @staticmethod
    def verifica_estabilidade(estabilidade_temp, valor_peca, valor_temp):
        estabilidade = estabilidade_temp
        if valor_temp != 0:
            if ((valor_temp % 25 == 0 and Util.sinal(valor_peca) == Util.sinal(valor_temp)) or valor_temp == 2):
                estabilidade = 5
                return estabilidade
            else:
                estabilidade += 1
                return estabilidade
        else:
            return estabilidade

    @staticmethod
    def verifica_linha(tabuleiro, x):
        for i in range(0, 8):
            if (tabuleiro.get_cor_peca(x, i) == 0):
                return False
        return True

    @staticmethod
    def verifica_coluna(tabuleiro, y):
        for i in range(0, 8):
            if (tabuleiro.get_cor_peca(i, y) == 0):
                return False
        return True

    @staticmethod
    def verifica_diag_prim(tabuleiro, x, y):
        while (tabuleiro.dentro_limites(x, y)):
            x += 1
            y -= 1

        x -= 1
        y += 1

        while (tabuleiro.dentro_limites(x, y)):
            if (tabuleiro.get_cor_peca(x, y) == 0):
                return False
            x -= 1
            y += 1
        return True

    @staticmethod
    def verifica_diag_sec(tabuleiro, x, y):
        while (tabuleiro.dentro_limites(x, y)):
            x -= 1
            y += 1

        x += 1
        y -= 1

        while (tabuleiro.dentro_limites(x, y)):
            if (tabuleiro.get_cor_peca(x, y) == 0):
                return False
            x += 1
            y -= 1
        return True

    @staticmethod
    def verifica_peca(self, tabuleiro, x, y):

        if (x < 0 or x > 7 or y < 0 or y > 7):
            return 2

        valor_peca = tabuleiro.get_cor_peca(x, y)
        estabilidade_vert = 0
        estabilidade_hori = 0
        estabilidade_diag_prim = 0
        estabilidade_diag_sec = 0

        if (valor_peca != 0 and (valor_peca % 5) != 0 and valor_peca != 2):

            valor_peca *= 5
            tabuleiro.set_cor_peca(x, y, valor_peca)

            valor_temp = self.verifica_peca(tabuleiro, x - 1, y + 1)

            estabilidade_diag_prim = self.verifica_estabilidade(estabilidade_diag_prim, valor_peca, valor_temp)

            valor_temp = self.verifica_peca(tabuleiro, x - 1, y)

            estabilidade_vert = self.verifica_estabilidade(estabilidade_vert, valor_peca, valor_temp)

            valor_temp = self.verifica_peca(tabuleiro, x - 1, y - 1)

            estabilidade_diag_sec = self.verifica_estabilidade(estabilidade_diag_sec, valor_peca, valor_temp)

            valor_temp = self.verifica_peca(tabuleiro, x, y - 1)

            estabilidade_hori = self.verifica_estabilidade(estabilidade_hori, valor_peca, valor_temp)

            if (estabilidade_diag_prim < 5):
                valor_temp = self.verifica_peca(tabuleiro, x + 1, y - 1)
                estabilidade_diag_prim = self.verifica_estabilidade(estabilidade_diag_prim, valor_peca, valor_temp)

            if (estabilidade_vert < 5):
                valor_temp = self.verifica_peca(tabuleiro, x + 1, y)
                estabilidade_vert = self.verifica_estabilidade(estabilidade_vert, valor_peca, valor_temp)

            if (estabilidade_diag_sec < 5):
                valor_temp = self.verifica_peca(tabuleiro, x + 1, y + 1)
                estabilidade_diag_sec = self.verifica_estabilidade(estabilidade_diag_sec, valor_peca, valor_temp)

            if (estabilidade_hori < 5):
                valor_temp = self.verifica_peca(tabuleiro, x, y + 1)
                estabilidade_hori = self.verifica_estabilidade(estabilidade_hori, valor_peca, valor_temp)

            if (estabilidade_diag_prim > 1 and estabilidade_vert > 1 and estabilidade_diag_sec > 1 and estabilidade_hori > 1):

                if (estabilidade_diag_prim != 5):
                    if (self.verifica_diag_prim(tabuleiro, x, y)):
                        estabilidade_diag_prim = 5

                if (estabilidade_vert != 5):
                    if (self.verifica_coluna(tabuleiro, y)):
                        estabilidade_vert = 5

                if (estabilidade_diag_sec != 5):
                    if (self.verifica_diag_sec(tabuleiro, x, y)):
                        estabilidade_diag_sec = 5

                if (estabilidade_hori != 5):
                    if (self.verifica_linha(tabuleiro, x)):
                        estabilidade_hori = 5

                if (estabilidade_diag_prim == 5 and estabilidade_vert == 5 and estabilidade_diag_sec == 5 and estabilidade_hori == 5):
                    valor_peca *= 5
                    tabuleiro.set_cor_peca(x, y, valor_peca)
                    tabuleiro.set_alterado(True)
                    return valor_peca

            return valor_peca
        else:
            return valor_peca
