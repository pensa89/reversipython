from util.Util import Util

class GerenciaPecas:

    def vira_pecas(self, tabuleiro, cor, linha, coluna):
        self.vira_pecas_verticalmente(tabuleiro, cor, linha, coluna)
        self.vira_pecas_horizontalmente(tabuleiro, cor, linha, coluna)
        self.vira_pecas_diagonal_principal(tabuleiro, cor, linha, coluna)
        self.vira_pecas_diagonal_secundaria(tabuleiro, cor, linha, coluna)
        tabuleiro.matriz[linha][coluna] = cor

    def vira_pecas_verticalmente(self, tabuleiro, cor, linha, coluna):
        incremento = 1
        while Util.checa_limites(linha, incremento) and Util.sinal(tabuleiro.matriz[linha + incremento][coluna]) == Util.sinal(-cor):
            incremento += 1
        if Util.checa_limites(linha, incremento) and Util.sinal(tabuleiro.matriz[linha + incremento][coluna]) == Util.sinal(cor):
            for i in range(1, incremento):
                tabuleiro.matriz[linha + i][coluna] = cor

        incremento = -1
        while Util.checa_limites(linha, incremento) and Util.sinal(tabuleiro.matriz[linha + incremento][coluna]) == Util.sinal(-cor):
            incremento -= 1
        if Util.checa_limites(linha, incremento) and Util.sinal(tabuleiro.matriz[linha + incremento][coluna]) == Util.sinal(cor):
            for i in range(-1, incremento, -1):
                tabuleiro.matriz[linha + i][coluna] = cor

    def vira_pecas_horizontalmente(self, tabuleiro, cor, linha, coluna):
        incremento = 1
        while Util.checa_limites(coluna, incremento) and Util.sinal(tabuleiro.matriz[linha][coluna + incremento]) == Util.sinal(-cor):
            incremento += 1
        if Util.checa_limites(coluna, incremento) and Util.sinal(tabuleiro.matriz[linha][coluna + incremento]) == Util.sinal(cor):
            for i in range(1, incremento):
                tabuleiro.matriz[linha][coluna + i] = cor
        incremento = -1
        while Util.checa_limites(coluna, incremento) and Util.sinal(tabuleiro.matriz[linha][coluna + incremento]) == Util.sinal(-cor):
            incremento -= 1
        if Util.checa_limites(coluna, incremento) and Util.sinal(tabuleiro.matriz[linha][coluna + incremento]) == Util.sinal(cor):
            for i in range(-1, incremento, -1):
                tabuleiro.matriz[linha][coluna + i] = cor

    def vira_pecas_diagonal_principal(self, tabuleiro, cor, linha, coluna):
        incremento_linha = -1
        incremento_coluna = -1
        while Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                        Util.sinal(tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(-cor):
            incremento_linha -= 1
            incremento_coluna -= 1
        if Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                        Util.sinal(tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(cor):
            while incremento_linha < -1 and incremento_coluna < -1:
                incremento_linha += 1
                incremento_coluna += 1
                tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna] = cor
        incremento_linha = 1
        incremento_coluna = 1
        while Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                        Util.sinal(tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(-cor):
            incremento_linha += 1
            incremento_coluna += 1
        if Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                        Util.sinal(tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(cor):
            while incremento_linha > 1 and incremento_coluna > 1:
                incremento_linha -= 1
                incremento_coluna -= 1
                tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna] = cor

    def vira_pecas_diagonal_secundaria(self, tabuleiro, cor, linha, coluna):
        incremento_linha = 1
        incremento_coluna = -1
        while Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                        Util.sinal(tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(-cor):
            incremento_linha += 1
            incremento_coluna -= 1
        if Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                        Util.sinal(tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(cor):
            while incremento_linha > 1 and incremento_coluna < -1:
                incremento_linha -= 1
                incremento_coluna += 1
                tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna] = cor

        incremento_linha = -1
        incremento_coluna = 1
        while Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                        Util.sinal(tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(-cor):
            incremento_linha -= 1
            incremento_coluna += 1
        if Util.checa_limites(linha, incremento_linha) and Util.checa_limites(coluna, incremento_coluna) and \
                        Util.sinal(tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna]) == Util.sinal(cor):
            while incremento_linha < -1 and incremento_coluna > 1:
                incremento_linha += 1
                incremento_coluna -= 1
                tabuleiro.matriz[linha + incremento_linha][coluna + incremento_coluna] = cor
