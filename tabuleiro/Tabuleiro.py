class Tabuleiro:

    def __init__(self):
        self.tamanho = 8
        self.matriz = [[0 for x in range(self.tamanho)] for x in range(self.tamanho)]
        self.turno = -1
        self.alterado = False

    def muda_turno(self):
        self.turno *= -1

    def dentro_limites(self, linha, coluna):
        return (linha >= 0 and linha < self.tamanho) and (coluna >= 0 and coluna < self.tamanho)

    def get_cor_peca(self, linha, coluna):
        return self.matriz[linha][coluna]

    def set_cor_peca(self, linha, coluna, valor):
        self.matriz[linha][coluna] = valor

    def set_alterado(self, alterado):
        self.alterado = alterado
