from tabuleiro.gerencia.GerenciaTabuleiro import GerenciaTabuleiro
from util.Util import Util
import sys

class ChecaVitoria:

    # tabuleiro é um tabuleiro estável
    @staticmethod
    def checa_vitoria(tabuleiro):
        soma = 0
        tamanho_tabuleiro = tabuleiro.tamanho
        gerente_tabuleiro = GerenciaTabuleiro(tabuleiro)

        if not len(gerente_tabuleiro.gera_movimentos_possiveis(1)) == 0 or not len(gerente_tabuleiro.gera_movimentos_possiveis(-1)) == 0:
            return -sys.maxsize
        else:
            for i in range(tamanho_tabuleiro):
                for j in range(tamanho_tabuleiro):
                    if tabuleiro.matriz[i][j] != 0:
                        soma = soma + tabuleiro.matriz[i][j]
            return Util.sinal(soma)
