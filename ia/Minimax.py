import sys
from random import randint

from ia.ParAcaoValor import ParAcaoValor
from tabuleiro.Tabuleiro import Tabuleiro
from tabuleiro.ChecaVitoria import ChecaVitoria
from tabuleiro.gerencia.GerenciaTabuleiro import GerenciaTabuleiro
from tabuleiro.gerencia.GerenciaPecas import GerenciaPecas
from util.Util import Util


class Minimax:

    def __init__(self):
        self.tabuleiros_explorados = set()

    def procura_alfa_beta(self, tabuleiro, profundidade):
        acoes = GerenciaTabuleiro(tabuleiro).gera_movimentos_possiveis(tabuleiro.turno) # retorna um set
        pares_acao_valor = []
        self.tabuleiros_explorados.add(tabuleiro)

        melhores_acoes = []
        melhor_acao = ParAcaoValor(None, -sys.maxsize)
        melhores_acoes.append(melhor_acao)

        indice = 0
        for acao_atual in set(acoes):
            tabuleiro_temp = Util.copia_profunda(tabuleiro)
            pares_acao_valor.append(ParAcaoValor(acao_atual, self.valor_minimo(tabuleiro_temp, -sys.maxsize, sys.maxsize, profundidade - 1)))

            if pares_acao_valor[indice].valor > melhores_acoes[0].valor:
                melhores_acoes.clear()
                melhores_acoes.append(pares_acao_valor[indice])
            elif pares_acao_valor[indice].valor == melhores_acoes[0].valor:
                melhores_acoes.append(pares_acao_valor[indice])

        self.tabuleiros_explorados.clear()
        return melhores_acoes[randint(0, len(melhores_acoes)-1)]

    def valor_maximo(self, tabuleiro, alfa, beta, profundidade):
        tabuleiro_estavel = Tabuleiro() # essa linha mudará
        # a linha acima receberá o algoritmo de peças estáveis
        if tabuleiro_estavel in self.tabuleiros_explorados:
            return -sys.maxsize
        if profundidade == 0 or ChecaVitoria.checa_vitoria(tabuleiro_estavel) == tabuleiro.turno:
            return self.funcao_utilidade(tabuleiro_estavel)

        self.tabuleiros_explorados.add(tabuleiro)
        tabuleiro_estavel.muda_turno()
        valor = -sys.maxsize
        acoes = GerenciaTabuleiro(tabuleiro).gera_movimentos_possiveis(tabuleiro.turno)

        for acao in acoes:
            tabuleiro_temp = Util.copia_profunda(tabuleiro)
            valor = max(valor, self.valor_minimo(tabuleiro_temp, alfa, beta, profundidade - 1))

            if valor >= beta:
                return valor
            alfa = max(alfa, valor)
        return valor

    def valor_minimo(self, tabuleiro, alfa, beta, profundidade):
        tabuleiro_estavel = Tabuleiro() # essa linha mudará
        # a linha acima receberá o algoritmo de peças estáveis
        if tabuleiro_estavel in self.tabuleiros_explorados:
            return sys.maxsize
        if profundidade == 0 or ChecaVitoria.checa_vitoria(tabuleiro_estavel) == tabuleiro.turno:
            return self.funcao_utilidade(tabuleiro_estavel)

        self.tabuleiros_explorados.add(tabuleiro)
        tabuleiro_estavel.muda_turno()
        valor = sys.maxsize
        acoes = GerenciaTabuleiro(tabuleiro).gera_movimentos_possiveis(tabuleiro.turno)

        for acao in acoes:
            tabuleiro_temp = Util.copia_profunda(tabuleiro)
            valor = min(valor, self.valor_maximo(tabuleiro_temp, alfa, beta, profundidade - 1))

            if valor <= alfa:
                return valor
            alfa = min(beta, valor)
        return valor

    def funcao_utilidade(self, tabuleiro_estavel):
        resultado = 0
        cor = tabuleiro_estavel.turno
        tamanho = tabuleiro_estavel.tamanho

        peca_estavel = 25 * cor # valor no tabuleiro de uma peça que não mudará de cor
        peso_canto = 10 # peso de uma peça de um dos cantos do tabuleiro
        peso_peca_estavel = 5 # peso de uma peça que não mudará de cor
        peso_peca_qualquer = 1 # peso de uma peça que não é estável
        if tabuleiro_estavel.matriz[0][0] == cor:
            resultado += peso_canto
        if tabuleiro_estavel.matriz[0][tamanho - 1] == cor:
            resultado += peso_canto
        if tabuleiro_estavel.matriz[tamanho - 1][0] == cor:
            resultado += peso_canto
        if tabuleiro_estavel.matriz[tamanho - 1][tamanho - 1] == cor:
            resultado += peso_canto

        for i in range(tamanho):
            for j in range(tamanho):
                if tabuleiro_estavel.matriz[i][j] == cor:
                    resultado += peso_peca_qualquer
                elif tabuleiro_estavel.matriz[i][j] == peca_estavel:
                    resultado += peso_peca_estavel
        return resultado

    def faz_jogada(self, tabuleiro, jogada): # jogada é um Ponto
        novo_tabuleiro = Util.copia_profunda(tabuleiro)
        GerenciaPecas.vira_pecas(novo_tabuleiro, novo_tabuleiro.turno, jogada.x, jogada.y)
        return novo_tabuleiro
