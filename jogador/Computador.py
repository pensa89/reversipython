from jogador.Jogador import Jogador
from ia.Minimax import Minimax
from util.Util import Util


class Computador(Jogador):

    def __init__(self, cor, dificuldade):
        super(Computador, self).__init__(cor)
        self.minimax = Minimax()
        self.dificuldade = dificuldade

    def faz_jogada_computador(self, tabuleiro):
        tabuleiro_temp = Util.copia_profunda(tabuleiro)
        proxima_jogada = self.minimax.procura_alfa_beta(tabuleiro_temp, self.dificuldade)
        return proxima_jogada
