from tabuleiro.gerencia.GerenciaPecas import GerenciaPecas

class Jogador:

    def __init__(self, cor):
        self.cor = cor
        self.pode_jogar = True

    def faz_jogada(self, tabuleiro, linha, coluna):
        GerenciaPecas().vira_pecas(tabuleiro, self.cor, linha, coluna)
        return True
