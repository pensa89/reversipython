from jogador.Jogador import Jogador
from tabuleiro.gerencia.GerenciaTabuleiro import GerenciaTabuleiro
from util.Ponto import Ponto


class Humano(Jogador):

    def __init__(self, cor):
       super(Humano, self).__init__(cor)

    def faz_jogada_humano(self, tabuleiro, linha, coluna):
        movimentos_possiveis = set()
        movimentos_possiveis = GerenciaTabuleiro(tabuleiro).gera_movimentos_possiveis(self.cor)
        for movimento in set(movimentos_possiveis):
            if movimento.x == linha and movimento.y == coluna:
                self.faz_jogada(tabuleiro, linha, coluna)
                tabuleiro.turno *= -1
                break
        return movimentos_possiveis
